package de.jeeschulung.api;

import java.util.Collection;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import de.jeeschulung.exercise.domain.Location;
import de.jeeschulung.exercise.domain.LocationServiceBean;

@Named
@RequestScoped
@Path("/locations")
public class LocationResource {
	
	@EJB
	private LocationServiceBean locationService;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAll(){
		Collection<Location> findAll = locationService.findAll();
		return findAll.isEmpty() ? Response.noContent().build() : Response.ok(findAll).build();
	}
	

}
