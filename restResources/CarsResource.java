package de.jeeschulung.api;

import java.util.Collection;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import de.jeeschulung.exercise.domain.Car;
import de.jeeschulung.exercise.domain.CarServiceBean;


@Named
@RequestScoped
@Path("/cars")
public class CarsResource {
	
	@EJB
	private CarServiceBean carService;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<Car> getAll(){
		return carService.findAll();
	}
	
	
	
}
