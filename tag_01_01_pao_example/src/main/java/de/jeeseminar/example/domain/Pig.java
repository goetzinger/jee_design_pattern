package de.jeeseminar.example.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@NamedQueries(
		@NamedQuery(name=Pig.QUERY_FIND_BY_NAME, query = Pig.QUERY_FIND_BY_NAME)
		)
public class Pig implements Serializable{
	
	public static final String PARAM_NAME = "name";
	public static final String QUERY_FIND_BY_NAME = "SELECT p FROM Pig p WHERE p.name = :" + PARAM_NAME;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@ElementCollection
	private List<String> tags;
	
	private String name;
	
	private int weight;
	
	public void setTags(List<String> tags) {
		this.tags = tags;
	}
	
	public List<String> getTags() {
		return tags;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public int getId() {
		return id;
	}

	@Override
	public String toString() {
		return "Pig [id=" + id + ", name=" + name + ", weight=" + weight + "]";
	}
	
	

}
