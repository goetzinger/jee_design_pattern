package de.jeeschulung.web;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Observable;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import de.jeeschulung.exercise.LocationDTO;

@RequestScoped
@Named
public class Locations extends Observable implements Serializable {

   
    private List<LocationDTO> locations = new ArrayList<LocationDTO>();
    
    private LocationDTO selected;
    
    @PostConstruct
    public void fillArray() {
    	locations.add(new LocationDTO(1,"Stuttgart"));
    	locations.add(new LocationDTO(2, "München"));
    }

    public List<LocationDTO> getLocations(){
    	return locations;
    }

    public LocationDTO getSelected() {
        return selected;
    }

    public void setSelected(long locationId) {
        this.selected = this.locations.stream().filter(loc -> loc.getId() == locationId).findFirst().get();
        super.setChanged();
        super.notifyObservers(selected.getId());
        super.clearChanged();
    }


}
