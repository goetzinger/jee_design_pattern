package de.jeeschulung.web;

import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

import de.jeeschulung.exercise.CarDTO;
import de.jeeschulung.exercise.CarLocationFacadeRemote;
import de.jeeschulung.exercise.LocationDTO;

@ApplicationScoped
@Named
public class CarLocationFacadeClient implements CarLocationFacadeRemote{

	CarLocationFacadeRemoteClient feignStub;
	@PostConstruct
	public void constrcutFeign(){
		feignStub = Feign.builder()
        .decoder(new GsonDecoder())
        .target(CarLocationFacadeRemoteCleint.class, "http://localhost:8080/api);
	}
	@Override
	public List<CarDTO> findAllCars() {
		return feignStub.findAllCars()
	}
	@Override
	public Optional<CarDTO> findCarById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public CarDTO insertCar(CarDTO toInsert) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public CarDTO udpateCar(CarDTO toInsert) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void deleteCar(long id) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public List<CarDTO> findCarByBrand(String brand) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public List<CarDTO> findCarByLocation(long locationId) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public List<LocationDTO> findAllLocations() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Optional<LocationDTO> findLocationById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public LocationDTO updateLocation(LocationDTO updatedValue) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public LocationDTO insertLocation(LocationDTO toInsert) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void deleteLocation(long id) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public Optional<LocationDTO> findByCar(long carId) {
		// TODO Auto-generated method stub
		return null;
	}
	
	

}
