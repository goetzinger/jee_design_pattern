package de.jeeschulung.web;

import java.util.List;

import de.jeeschulung.exercise.CarDTO;
import de.jeeschulung.exercise.CarLocationFacadeRemote;

public interface CarLocationFacadeRemoteClient extends CarLocationFacadeRemote{
	
	
	@Override
	@RequestLine("GET /cars")
	 List<CarDTO> findAllCars() ;

}
