package de.jeeschulung.web;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@Named
@RequestScoped
public class PageB {

	
	public String next() {
		return System.currentTimeMillis() % 2 == 0 ? "successful" : "error"; 
	}
	
	public String previous() {
		return "successful"; 
	}
	

}
