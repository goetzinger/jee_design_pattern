package de.jeeschulung.web;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import de.jeeschulung.exercise.CarDTO;
import de.jeeschulung.exercise.LocationDTO;

@RequestScoped
@Named
public class CarLocation implements Serializable, Observer {

	@Inject
	private Cars cars;

	@Inject
	private Locations locations;

	private String some;

	private HashMap<Long, List<CarDTO>> locationIdToCar = new HashMap<>();

	@PostConstruct
	public void fillMap() {
		locations.addObserver(this);

		List<LocationDTO> locationsList = locations.getLocations();
		locationIdToCar.put(locationsList.get(0).getId(), generateCar1List());
		locationIdToCar.put(locationsList.get(1).getId(), generateCar2List());
	}
	
	@PreDestroy
	public void unregister() {
		locations.deleteObserver(this);
	}

	public String getSome() {
		return some;
	}

	public void setSome(String some) {
		this.some = some;
	}

	private List<CarDTO> generateCar1List() {
		List<CarDTO> cars = new ArrayList();
		cars.add(new CarDTO(1, "VW", "ID3", 150, 200));
		cars.add(new CarDTO(2, "Hyundai", "Ioniq 5", 180, 230));
		return cars;
	}

	private List<CarDTO> generateCar2List() {
		List<CarDTO> cars = new ArrayList();
		cars.add(new CarDTO(1, "BMW", "3er", 150, 200));
		cars.add(new CarDTO(2, "Mercedes", "CLA", 180, 230));
		return cars;
	}

	public void showCarsOf(Long locationId) {
		cars.setCars(locationIdToCar.get(locationId));
	}

	public String someother() {
		System.out.println("Other");
		return "";
	}

	@Override
	public void update(Observable o, Object locationId) {
		if (locationId instanceof Long)
			showCarsOf((Long) locationId);
	}

}
