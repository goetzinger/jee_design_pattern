package de.jeeschulung.web;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import de.jeeschulung.exercise.CarDTO;

@RequestScoped
@Named
public class Cars implements Serializable {

	
	@Inject
	CarLocationFacadeClient carLocationBD;
   
    private List<CarDTO> cars = new ArrayList<CarDTO>();
    private CarDTO selected;
    
    @PostConstruct
    public void fillArray() {
    	cars.add(new CarDTO(1, "VW", "ID3", 150, 200));
    	cars.add(new CarDTO(2,"Hyundai","Ioniq 5",180,230));
    }

    public List<CarDTO> getCars(){
    	return cars;
    }
    
    public void setCars(List<CarDTO> cars) {
		this.cars = cars;
	}

    public CarDTO getSelected() {
        return selected;
    }

    public void setSelected(CarDTO selected) {
        this.selected = selected;
    }




}
