package de.jeeseminar.example.domain;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
@LocalBean
public class PigService {

	@PersistenceContext
	private EntityManager manager;
	
	
	public Pig insert(Pig toInsert){
		if(toInsert == null)
			throw new IllegalArgumentException("Should not be null");
		manager.persist(toInsert);
		return toInsert;
	}
	
}
