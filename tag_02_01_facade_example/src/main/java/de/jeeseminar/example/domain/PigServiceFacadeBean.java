package de.jeeseminar.example.domain;

import java.lang.reflect.InvocationTargetException;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.BeanUtils;

import de.jeeseminar.example.api.PigServiceFacade;
import de.jeeseminar.example.api.PigServiceFacadeRemote;

@Stateless
@Local(PigServiceFacade.class)
@Remote(PigServiceFacadeRemote.class)
public class PigServiceFacadeBean implements PigServiceFacadeRemote{
	
	@Resource
	private SessionContext context;
	
	@EJB
	private PigService service;
	
	@Override
	public de.jeeseminar.example.api.Pig insert(de.jeeseminar.example.api.Pig dto) {
		try {
			//PigService lookup = (PigService) context.lookup("java:global/tag_02_01_facade_example/PigService");
			Pig entity = new Pig();
			if(dto.getWeight() > 10)
				throw new IllegalArgumentException("Pig should have a weight");
			BeanUtils.copyProperties(entity,dto);
			entity = service.insert(entity);
			BeanUtils.copyProperties(dto, entity);
			return dto;
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
