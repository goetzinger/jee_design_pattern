package de.jeeschulung.exercise.domain;

import java.lang.reflect.Field;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.jeeseminar.example.domain.Pig;
import de.jeeseminar.example.domain.PigService;

public class PigServiceTest {

	
	private PigService testCandidate = new PigService();
	
	private static EntityManagerFactory entityManagerFactory;

	@BeforeClass
	public static void setupEMF() {
		entityManagerFactory = Persistence.createEntityManagerFactory("projectUnit");
		
	}
	private EntityManager em;
	@Before
	public void setupEM() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		em = entityManagerFactory.createEntityManager();
		em.getTransaction().begin();
		Field managerField = PigService.class.getDeclaredField("manager");
		managerField.setAccessible(true);
		managerField.set(testCandidate, em);
	}
	
	@Test
	public void doSomeTestHere() {
		Pig missPiggy = new Pig();
		missPiggy.setName("Miss Piggy");
		missPiggy.setWeight(50);
		
		testCandidate.insert(missPiggy);
		
		TypedQuery<Pig> searchByName = em.createNamedQuery(Pig.QUERY_FIND_BY_NAME, Pig.class);
		List<Pig> resultList = searchByName.setParameter(Pig.PARAM_NAME, "Miss Piggy").getResultList();
		Assert.assertEquals(missPiggy, resultList.get(0));
	}
	
	@After
	public void closeEM() {
		em.getTransaction().rollback();
		em.close();
	}
	
	@AfterClass
	public static void teardownEMF() {
		entityManagerFactory.close();
	}
	
}
