package de.jeeschulung.exercise.view;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@RequestScoped
@Named("right")
public class RightBean {

	private String output = "Leer";

	@PostConstruct
	public void initInfos() {
	}

	public String getOutput() {
		return output;
	}

	public void setOutput(String output) {
		this.output = output;
	}

}
