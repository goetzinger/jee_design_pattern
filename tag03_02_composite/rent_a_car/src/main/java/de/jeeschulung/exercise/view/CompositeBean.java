package de.jeeschulung.exercise.view;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

@RequestScoped
@Named
public class CompositeBean implements SelectionListener{

	@Inject
	private LeftBean leftBean;
	
	@Inject
	private RightBean rightBean;
	
	private String status = "";
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@PostConstruct
	public void register() {
		leftBean.register(this);
	}
	
	@PreDestroy
	public void unregister() {
		leftBean.unregister(this);
	}

	@Override
	public void selected(String entry) {
		rightBean.setOutput(entry);
		
	}
	
}
