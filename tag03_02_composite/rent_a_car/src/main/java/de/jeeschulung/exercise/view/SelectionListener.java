package de.jeeschulung.exercise.view;

public interface SelectionListener {
	
	 void selected(String entry);

}
