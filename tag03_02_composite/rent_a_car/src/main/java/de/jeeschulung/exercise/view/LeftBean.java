package de.jeeschulung.exercise.view;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@RequestScoped
@Named("left")
public class LeftBean {

	private List<String> infos = new ArrayList<>();
	
	private DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME;

	private List<SelectionListener> listeners = new ArrayList<>();
	
	
	@PostConstruct
	public void initInfos() {
		infos.add("Erster");
		infos.add("Zweiter");
		infos.add("Dritter");
		infos.add("vierter");
	}
	
	public String getTime() {
		return formatter.format(LocalDateTime.now());
	}
	
	public List<String> getInfos() {
		return infos;
	}

	public void register(SelectionListener listener) {
		this.listeners .add(listener);
	}
	
	public void selected(String selectedElement) {
		this.listeners.stream().forEach(l -> l.selected(selectedElement));
	}
	
	public void unregister(SelectionListener listener) {
		
		this.listeners.remove(listener);
	}
}
