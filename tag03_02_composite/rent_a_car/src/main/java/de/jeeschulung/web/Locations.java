package de.jeeschulung.web;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import de.jeeschulung.exercise.LocationDTO;

@SessionScoped
@Named
public class Locations implements Serializable {

   
    private List<LocationDTO> locations = new ArrayList<LocationDTO>();
    private LocationDTO selected;
    
    @PostConstruct
    public void fillArray() {
    	locations.add(new LocationDTO(1,"Stuttgart"));
    	locations.add(new LocationDTO(2, "München"));
    }

    public List<LocationDTO> getLocations(){
    	return locations;
    }

    public LocationDTO getSelected() {
        return selected;
    }

    public void setSelected(LocationDTO selected) {
        this.selected = selected;
    }


}
