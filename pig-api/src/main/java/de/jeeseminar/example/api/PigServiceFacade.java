package de.jeeseminar.example.api;

public interface PigServiceFacade {

	Pig insert(Pig toInsert);

}
