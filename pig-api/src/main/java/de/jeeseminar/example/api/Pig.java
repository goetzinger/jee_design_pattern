package de.jeeseminar.example.api;

import java.io.Serializable;
import java.util.List;


public class Pig implements Serializable{
	
	private int id;
	
	private List<String> tags;
	
	private String name;
	
	private int weight;
	
	public void setTags(List<String> tags) {
		this.tags = tags;
	}
	
	public List<String> getTags() {
		return tags;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public int getId() {
		return id;
	}

	@Override
	public String toString() {
		return "Pig [id=" + id + ", name=" + name + ", weight=" + weight + "]";
	}
	
	

}
