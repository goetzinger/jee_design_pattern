package de.jeeschulung.exercise.domain;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class ORMappingTest {

	
	private static EntityManagerFactory entityManagerFactory;

	@BeforeClass
	public static void setupEMF() {
		entityManagerFactory = Persistence.createEntityManagerFactory("projectUnit");
		
	}
	private EntityManager em;
	@Before
	public void setupEM() {
		em = entityManagerFactory.createEntityManager();
		em.getTransaction().begin();
	}
	
	@Test
	public void doSomeTestHere() {
		
	}
	
	@After
	public void closeEM() {
		em.getTransaction().rollback();
		em.close();
	}
	
	@AfterClass
	public static void teardownEMF() {
		entityManagerFactory.close();
	}
	
}
